import credentialProvidersPkg from '@aws-sdk/credential-providers';
const { fromIni } = credentialProvidersPkg;

const credential = fromIni( {
    profile: 'main'
});

import ec2Pkg from '@aws-sdk/client-ec2';
const { EC2 } = ec2Pkg;

const ec2 = new EC2( {
    region: 'eu-west-2',
    credentials: credential
});

try {
    const describedInstances = await ec2.describeInstances('');
    console.log(describedInstances.Reservations[0].Instances[0].InstanceId);
} catch (error) {
    console.log(error);
}